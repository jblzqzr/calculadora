def sumar(a, b):
    return a + b


def restar(a, b):
    return a - b


if __name__ == '__main__':
    print("La suma de {} + {} = {}".format(1, 2, sumar(1, 2)))
    print("La suma de {} + {} = {}".format(3, 4, sumar(3, 4)))
    print("La resta de {} - {} = {}".format(6, 5, restar(6, 5)))
    print("La resta de {} - {} = {}".format(8, 7, restar(8, 7)))
